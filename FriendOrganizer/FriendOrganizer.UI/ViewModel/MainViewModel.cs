﻿using FriendOrganizer.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FriendOrganizer.UI.ViewModel
{
    public class MainViewModel: ViewModelBase
    {
        public Data.IFriendDataService _friendDataService;

        //notify when the collection changes
        public ObservableCollection<Model.Friend> Friends { get; set; }

        private Friend _selectedFriend;

       

        public Friend SelectedFriend
        {
            get { return _selectedFriend; }
            set {
                
                _selectedFriend = value;
                //OnPropertyChanged("SelectedFriend");
                OnPropertyChanged(nameof(SelectedFriend));
            
            }
        }


        public MainViewModel(Data.IFriendDataService friendDataService)
        {
            Friends = new ObservableCollection<Friend>();
            _friendDataService = friendDataService;
        }

        
        public void Load()
        {
            var friends = _friendDataService.GetAll();
            Friends.Clear();
            foreach(var friend in friends)
            {
                Friends.Add(friend);
            }
        }

        

    }
}
