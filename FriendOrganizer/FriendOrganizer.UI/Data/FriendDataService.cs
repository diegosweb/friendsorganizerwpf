using System.Collections.Generic;
using FriendOrganizer.Model;

namespace FriendOrganizer.UI.Data
{
    public class FriendDataService : IFriendDataService
    {
        public IEnumerable<Friend> GetAll()
        {
            yield return new Friend { Id = 1, Email = "dschonhals@hotmail.com", FirstName = "Diego", LastName = "Schonhals" };
            yield return new Friend { Id = 2, Email = "gabrielaurrels@gmail.com", FirstName = "Gabriela", LastName = "Urrels" };
            yield return new Friend { Id = 3, Email = "rschonhals@gmail.com", FirstName = "Rodolfo", LastName = "Schonhals" };


        }
    }
}
